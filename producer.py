import json
from time import sleep

from kafka import KafkaProducer

if __name__ == '__main__':
    producer = KafkaProducer(bootstrap_servers=['localhost:9092'],
                             value_serializer=lambda v: json.dumps(v).encode('utf-8'))
    for i in range(1000):
        producer.send('test', {f"test{i}": i})
        sleep(1)
        print(f"sent {i}")
